import {ICustomer} from './custumer.model';

export interface ISubscription {
  id: string;
  customer_id: string;
  plan_id: string;
  plan_quantity: number;
  plan_unit_price: number;
  plan_amount: number;
  billing_period: number;
  billing_period_unit: string;
  plan_free_quantity: number;
  status: string;
  current_term_start: number;
  current_term_end: number;
  next_billing_at: number;
  created_at: number;
  started_at: number;
  activated_at: number;
  updated_at: number;
  resource_version: number;
  customer: ICustomer;
}


export class Sub implements ISubscription {
  constructor(
    public id: string,
    public customer_id: string,
    public plan_id: string,
    public plan_quantity: number,
    public plan_unit_price: number,
    public plan_amount: number,
    public billing_period: number,
    public billing_period_unit: string,
    public plan_free_quantity: number,
    public status: string,
    public current_term_start: number,
    public current_term_end: number,
    public next_billing_at: number,
    public created_at: number,
    public started_at: number,
    public activated_at: number,
    public updated_at: number,
    public resource_version: number,
    public customer: ICustomer,
  ) {}
}
