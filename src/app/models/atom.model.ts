import {ICustomer} from './custumer.model';
import {ISubscription} from './subscriptions.model';

export interface IAtom {
  customer: ICustomer;
  subscription: ISubscription;
}

export class Atom implements IAtom {
  constructor(
    public  customer: ICustomer,
    public subscription: ISubscription
  ) {}
}
