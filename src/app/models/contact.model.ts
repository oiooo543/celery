export interface IContact {
  id: string;
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
  orgname: string;
  sentcnt: number;
  created_timestamp: string;
  updated_timestamp: string;
  links: {
    'contactTags': string
  };
  tags?: any;
  notes?: any;
  tasks?: any;
}

export class Contact implements IContact {
  constructor(
    public id: string,
    public email: string,
    public firstName: string,
    public lastName: string,
    public orgname: string,
    public phone: string,
    public sentcnt: number,
    public created_timestamp: string,
    public updated_timestamp: string,
    public links: {
      'contactTags': string
    },
    public tags?: [string],
    public notes?: [string],
    public tasks?: [string]
  ) {}
}
