export interface ICustomer {
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
  orgname?: string;
  sentcnt?: number;
}

export class Customer implements ICustomer {
  constructor(
    public email: string,
    public phone: string,
    public firstName: string,
    public lastName: string,
    public orgname?: string,
    public sentcnt?: number,
  ) {}
}
