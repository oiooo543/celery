import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';



@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
       Api_Token: '2555482d6d8aba30df9c4028a3bfd6750ba9c9a7368b487853a95f3def2d0be1b2b4f22f',
      },
    });

    return next.handle(req);
  }
}
