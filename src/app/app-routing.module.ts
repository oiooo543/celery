import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from './shared/guard/auth.guard';
import {SecureInnerPagesGuard} from './shared/guard/secure-inner-pages.guard.ts.guard';
import {SubscriptionsComponent} from './subscriptions/subscriptions.component';
import {LeadsComponent} from './leads/leads.component';


const routes: Routes = [
  { path: 'home',  component: HomeComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'dashboard',  component: DashboardComponent, canActivate: [AuthGuard]  },
  { path: 'subs', component: SubscriptionsComponent, canActivate: [AuthGuard]},
  { path: 'contacts', component: LeadsComponent, canActivate: [AuthGuard]},
  { path: '',   redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
