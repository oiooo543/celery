import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';


import { createRequestOption } from '../services/utils/request.utils';
import { ISubscription } from '../models/subscriptions.model';
import { HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {IContact} from '../models/contact.model';


type EntityResponseType = HttpResponse<IContact>;
type EntityArrayResponseType = HttpResponse<ISubscription[]>;

@Injectable({ providedIn: 'root' })
export class LeadService {
  public resourceUrl = 'api/3/contacts';

  constructor(protected http: HttpClient) {}

  create(store: IContact): Observable<IContact> {
    const  bod = {
      contact: store
    };
    console.log(bod);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        API_Token: '2555482d6d8aba30df9c4028a3bfd6750ba9c9a7368b487853a95f3def2d0be1b2b4f22f'
      })
    };
    return this.http.post<IContact>(this.resourceUrl, bod, httpOptions);
  }

  update(store: IContact): Observable<IContact> {
     const  bod = {
      contact: store
    };
     console.log(bod);
     const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        API_Token: '2555482d6d8aba30df9c4028a3bfd6750ba9c9a7368b487853a95f3def2d0be1b2b4f22f'
      })
    };
     return this.http.put<IContact>(this.resourceUrl + '/' + store.id, {contact: store}, httpOptions);
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IContact>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    const head = {
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + btoa('live_41XjdkpXb2I8F0w8G7prFw3LhETC48Sg')
    };
    return this.http.get<ISubscription[]>(this.resourceUrl,  {  headers: head, params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  fetchAll(offset: number = 0): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        API_Token: '2555482d6d8aba30df9c4028a3bfd6750ba9c9a7368b487853a95f3def2d0be1b2b4f22f'
      })
    };
    return this.http.get('api/3/contacts?limit=100&offset=' + offset, httpOptions);
  }
}
