import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';


import { createRequestOption } from '../services/utils/request.utils';
import { ISubscription } from '../models/subscriptions.model';
import {IContact} from '../models/contact.model';
import {error} from 'util';


type EntityResponseType = HttpResponse<ISubscription>;
type EntityArrayResponseType = HttpResponse<ISubscription[]>;

@Injectable({ providedIn: 'root' })
export class SubscriptionService {
  public resourceUrl = 'https://celery.api-us1.com/api/3/';
  contacts: IContact[];
  constructor(protected http: HttpClient) {}

  create(store: ISubscription): Observable<EntityResponseType> {
    return this.http.post<ISubscription>(this.resourceUrl, store, { observe: 'response' });
  }

  update(store: ISubscription): Observable<EntityResponseType> {
    return this.http.put<ISubscription>(this.resourceUrl, store, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISubscription>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    const head = {
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + btoa('live_41XjdkpXb2I8F0w8G7prFw3LhETC48Sg')
    };
    return this.http.get<ISubscription[]>(this.resourceUrl,  {  headers: head, params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  fetchAll(): Observable<ISubscription[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Basic ' + btoa('live_41XjdkpXb2I8F0w8G7prFw3LhETC48Sg:')
      })
    };
    return this.http.get<ISubscription[]>('api/v2/subscriptions', httpOptions);
  }
}
