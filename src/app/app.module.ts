import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeadComponent } from './head/head.component';
import { HomeComponent } from './home/home.component';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {environment} from '../environments/environment';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {AuthService} from './services/auth.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {SubscriptionService} from './services/subscription.service';
import {KeysPipe, SubscriptionsComponent} from './subscriptions/subscriptions.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './shared/guard/auth.interceptor';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import { LeadsComponent } from './leads/leads.component';
import {LeadService} from './services/leads.service';


@NgModule({
  declarations: [
    AppComponent,
    HeadComponent,
    HomeComponent,
    DashboardComponent,
    LoginComponent,
    SubscriptionsComponent,
    KeysPipe,
    LeadsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    HttpClientModule,
    FormsModule,
    NgxSpinnerModule,
  ],
  providers: [
    AuthService,
    AngularFirestore,
    LeadService,
    SubscriptionService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
