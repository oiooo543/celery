import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {SubscriptionService} from '../services/subscription.service';
import {ISubscription} from '../models/subscriptions.model';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {Atom, IAtom} from '../models/atom.model';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {
  constructor(public subscription: SubscriptionService, private spinner: NgxSpinnerService) {
  }

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredList = this.listFilter ? this.performFilter(this.listFilter) : this.dList;
  }
  get listNFilter(): string {
    return this._listFilter;
  }
  set listNFilter(value: string) {
    this._listFilter = value;
    this.filteredList = this.listFilter ? this.performNFilter(this.listFilter) : this.dList;
  }

  subscriptions: IAtom [];
  ehospl: any;
  filteredList: any;
  // tslint:disable-next-line:variable-name
  _listFilter: string;
  dList = [];
  private prop: any;
  count = 0;

  loadAll() {
    this.subscription
      .fetchAll()
      .subscribe(
        (res: ISubscription[]) => {
          this.ehospl = res;
          //  this.filteredList = this.ehospl;
          Object.values(this.ehospl.list).forEach(value => {this.dList[this.count] = value; this.count++; });
          this.filteredList = this.dList;
          console.log(this.filteredList);
        }),
      // tslint:disable-next-line:no-unused-expression
      (res: HttpErrorResponse) => this.onError(res.message);
  }


performFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    return this.dList.filter((phist: IAtom) => phist.subscription.status.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  performNFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    return this.dList.filter((phist: IAtom) => phist.customer.email.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }
ngOnInit() {
    this.loadAll();
  }

  protected onError(errorMessage: string) {
    console.log(errorMessage, null, null);
  }

  onSelect(selectedItem: IAtom) {
    console.log(selectedItem.subscription.id);
  }
}

@Pipe({ name: 'keys',  pure: false })
export class KeysPipe implements PipeTransform {
  transform(key: any, args: any[] = null): any {
    return Object.values(key);
  }
}


