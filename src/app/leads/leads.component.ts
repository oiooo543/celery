import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {LeadService} from '../services/leads.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {IAtom} from '../models/atom.model';
import {HttpErrorResponse} from '@angular/common/http';
import {IContact} from '../models/contact.model';
import {of} from 'rxjs';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.css']
})
export class LeadsComponent implements OnInit {

  constructor(public leads: LeadService, private spinner: NgxSpinnerService) {
    this.loadAll();
  }

  get listFilter(): string {
    return this._listFilter;
  }


  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredList = this.listFilter ? this.performFilter(this.listFilter) : this.dList;
  }
  get listNFilter(): string {
    return this._listNFilter;
  }
  set listNFilter(value: string) {
    this._listNFilter = value;
    this.filteredList = this.listFilter ? this.performNFilter(this.listFilter) : this.dList;
  }
  subscriptions: IAtom [];
  ehospl: any;
  filteredList: any;
  // tslint:disable-next-line:variable-name
  _listFilter: string;
  // tslint:disable-next-line:variable-name
  _listNFilter: string;
  dList = [];
  // tslint:disable-next-line:variable-name
  from_date: string;
  // tslint:disable-next-line:variable-name
  to_date: string;
  private prop: any;
  count = 0;
  currentLimit = 0;
  currentOffset = 0;
  offset = 0;
  isAactive = false;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  id: string;
  appStatus: string;
  filterByDate(values) {
    console.log(values);
    this.filteredList = this.dList.filter(
      (m: IContact) => {
        return new Date(m.created_timestamp) >= new Date(values.from_date) && new Date(m.updated_timestamp) <= new Date(values.to_date);
      }
    );
  }
  getNextPage(offset) {
    console.log(this.currentOffset);
    console.log(this.currentLimit);
    console.log(offset);
    this.leads.fetchAll(offset)
      .subscribe(
        (res: any) => {
          this.count = res.meta.total;
          this.currentOffset = res.meta.page_input.offset;
          this.currentLimit = res.meta.page_input.limit;
          this.ehospl = res.contacts;
          // console.log(this.ehospl);
          //  this.filteredList = this.ehospl;
          Object.values(this.ehospl).forEach(value => {this.dList[this.count] = value; this.count++; });
          this.filteredList = this.dList;
          if (offset >= 100) {
            this.isAactive = true;
          } else {
            this.isAactive = false;
          }
         // console.log(this.filteredList);
        }),
      // tslint:disable-next-line:no-unused-expression
      (res: HttpErrorResponse) => this.onError(res.message);
  }
  loadAll() {
    this.leads
      .fetchAll()
      .subscribe(
        (res: any) => {
          this.count = res.meta.total;
          this.currentOffset = res.meta.page_input.offset;
          this.currentLimit = res.meta.page_input.limit;
          this.ehospl = res.contacts;
          console.log(this.ehospl);
          //  this.filteredList = this.ehospl;
          Object.values(this.ehospl).forEach(value => {this.dList[this.count] = value; this.count++; });
          this.filteredList = this.dList;
          console.log(this.filteredList);
        }),
      // tslint:disable-next-line:no-unused-expression
      (res: HttpErrorResponse) => this.onError(res.message);
  }
  performFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    return this.dList.filter((phist: IContact) => phist.firstName.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  performNFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    return this.dList.filter((phist: IContact) => phist.email.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }
  ngOnInit() {
    // this.loadAll();
  }

  protected onError(errorMessage: string) {
    console.log(errorMessage, null, null);
  }

  onSelect(selectedItem: IContact) {
    console.log(selectedItem.id);
    this.id = selectedItem.id;
    this.email = selectedItem.email;
    this.firstName = selectedItem.firstName;
    this.lastName = selectedItem.lastName;
    this.phone = selectedItem.phone;
  }
  saveOrUpdate(values: IContact) {
    console.log(values);
    if (values.id  == null) {
      this.leads.create(values).subscribe((contact) => console.log(contact),
        (error => console.log(error)));
    } else {
      this.leads.update(values).subscribe(newContact => console.log(newContact),
        error => console.log(error));
    }
  }
  onChange(value) {
    console.log(value);
    this.filteredList = this.dList.sort();
  }
}

@Pipe({ name: 'keys',  pure: false })
export class KeysPipe implements PipeTransform {
  transform(key: any, args: any[] = null): any {
    return Object.values(key);
  }

}
