export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCFkCJRrbpkALOIh8n8DF4X7PB4R9BQhRQ',
    authDomain: 'lery-app.firebaseapp.com',
    databaseURL: 'https://lery-app.firebaseio.com',
    projectId: 'lery-app',
    storageBucket: 'lery-app.appspot.com',
    messagingSenderId: '731320279990',
    appId: '1:731320279990:web:78238af28a41b17fe61717',
    measurementId: 'G-JVE6VSY22J'
  },
  urlChargeBee: 'https://celery.chargebee.com/api/v2/subscriptions'
};
