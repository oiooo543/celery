// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCFkCJRrbpkALOIh8n8DF4X7PB4R9BQhRQ',
    authDomain: 'lery-app.firebaseapp.com',
    databaseURL: 'https://lery-app.firebaseio.com',
    projectId: 'lery-app',
    storageBucket: 'lery-app.appspot.com',
    messagingSenderId: '731320279990',
    appId: '1:731320279990:web:78238af28a41b17fe61717',
    measurementId: 'G-JVE6VSY22J'
  },
  urlChargeBee: 'https://celery.chargebee.com/api/v2/subscriptions'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
